package jp.co.demo.address.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.demo.address.domain.dao.CustomerDao;
import jp.co.demo.address.domain.model.Customer;

@Service
public class CustomerService extends BaseService {

	@Autowired
	CustomerDao customerRepository;

	public List<Customer> getCustomers() {
		return customerRepository.selectAll();
	}
}
