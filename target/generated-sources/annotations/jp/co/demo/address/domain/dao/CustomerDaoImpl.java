package jp.co.demo.address.domain.dao;

/** */
@org.springframework.stereotype.Repository()
@javax.annotation.Generated(value = { "Doma", "2.6.2" }, date = "2016-11-01T15:36:12.331+0900")
public class CustomerDaoImpl extends org.seasar.doma.internal.jdbc.dao.AbstractDao implements jp.co.demo.address.domain.dao.CustomerDao {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.6.2");
    }

    private static final java.lang.reflect.Method __method0 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(jp.co.demo.address.domain.dao.CustomerDao.class, "selectAll");

    /**
     * @param config the config
     */
    @org.springframework.beans.factory.annotation.Autowired()
    public CustomerDaoImpl(org.seasar.doma.jdbc.Config config) {
        super(config);
    }

    @Override
    public java.util.List<jp.co.demo.address.domain.model.Customer> selectAll() {
        entering("jp.co.demo.address.domain.dao.CustomerDaoImpl", "selectAll");
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method0);
            __query.setMethod(__method0);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/jp/co/demo/address/domain/dao/CustomerDao/selectAll.sql");
            __query.setEntityType(jp.co.demo.address.domain.model._Customer.getSingletonInternal());
            __query.setCallerClassName("jp.co.demo.address.domain.dao.CustomerDaoImpl");
            __query.setCallerMethodName("selectAll");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.List<jp.co.demo.address.domain.model.Customer>> __command = getCommandImplementors().createSelectCommand(__method0, __query, new org.seasar.doma.internal.jdbc.command.EntityResultListHandler<jp.co.demo.address.domain.model.Customer>(jp.co.demo.address.domain.model._Customer.getSingletonInternal()));
            java.util.List<jp.co.demo.address.domain.model.Customer> __result = __command.execute();
            __query.complete();
            exiting("jp.co.demo.address.domain.dao.CustomerDaoImpl", "selectAll", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("jp.co.demo.address.domain.dao.CustomerDaoImpl", "selectAll", __e);
            throw __e;
        }
    }

}
